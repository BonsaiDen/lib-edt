// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::marker::PhantomData;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use glam::{Vec2, UVec2};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{Color, CanvasState, Vec2UV};
use crate::rasterizer::{clip::Clip, Rasterizer, PIXEL_ROUNDING};
use crate::fast::{
    read_rgba8_no_bounds,
    write_rgba8_no_bounds,
    read_argb32_no_bounds,
    write_argb32_no_bounds,
    f32_to_i32
};


// Traits ---------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub trait PxlType: Copy {
    const ELEMENTS: usize;
    fn read_color(buffer: &[Self], index: usize) -> Color;
    fn write_color(buffer: &mut [Self], index: usize, color: Color);
    fn clear_color(buffer: &mut [Self], color: Color);
}

impl PxlType for u8 {
    const ELEMENTS: usize = 4;

    #[inline(always)]
    fn read_color(buffer: &[u8], index: usize) -> Color {
        read_rgba8_no_bounds(buffer, index)
    }

    #[inline(always)]
    fn write_color(buffer: &mut [Self], index: usize, color: Color) {
        write_rgba8_no_bounds(buffer, index, color);
    }

    #[inline(always)]
    fn clear_color(buffer: &mut [Self], color: Color) {
        let color: [u8; 4] = color.into();
        for p in buffer.chunks_exact_mut(4) {
            p.copy_from_slice(&color);
        }
    }
}

impl PxlType for u32 {
    const ELEMENTS: usize = 1;

    #[inline(always)]
    fn read_color(buffer: &[u32], index: usize) -> Color {
        read_argb32_no_bounds(buffer, index)
    }

    #[inline(always)]
    fn write_color(buffer: &mut [Self], index: usize, color: Color) {
        write_argb32_no_bounds(buffer, index, color);
    }

    #[inline(always)]
    fn clear_color(buffer: &mut [Self], color: Color) {
        buffer.fill(color.into());
    }
}


// Abstract Pixel Drawing Buffer ----------------------------------------------
// ----------------------------------------------------------------------------
pub(crate) struct CanvasBuffer<'a, T: PxlType> {
    pixels: &'a mut [T],
    size: UVec2,
    depth: PhantomData<T>
}

impl<'a, T: PxlType> CanvasBuffer<'a, T> {
    pub fn new(pixels: &'a mut [T], size: UVec2) -> Self {
        Self {
            pixels,
            size,
            depth: PhantomData
        }
    }

    #[inline(always)]
    pub fn width(&self) -> u32 {
        self.size.x
    }

    #[inline(always)]
    pub fn height(&self) -> u32 {
        self.size.y
    }

    #[inline(always)]
    pub fn clear(&mut self, color: Color) {
        T::clear_color(self.pixels, color);
    }

    #[inline(always)]
    pub fn write_color(&mut self, index: usize, color: Color) {
        T::write_color(self.pixels, index, color)
    }

    #[inline(always)]
    pub fn read_color(&self, index: usize) -> Color {
        T::read_color(self.pixels, index)
    }

    pub fn pixel(&mut self, p: Vec2, state: &CanvasState) {
        let p = state.transform.transform_point2(p + Vec2::new(PIXEL_ROUNDING, PIXEL_ROUNDING));
        let x = f32_to_i32(p.x);
        let y = f32_to_i32(p.y);
        if x >= 0 && y >= 0 && x < self.size.x as i32 && y < self.size.y as i32 {
            Rasterizer::new(self, state).pixel(x as u32, y as u32);
        }
    }

    pub fn line(&mut self, a: Vec2, b: Vec2, state: &CanvasState, skip_last_pixel: bool) {
        let a = state.transform.transform_point2(a) + Vec2::new(PIXEL_ROUNDING, PIXEL_ROUNDING);
        let b = state.transform.transform_point2(b) + Vec2::new(PIXEL_ROUNDING, PIXEL_ROUNDING);

        if self.in_bounds(a) && self.in_bounds(b) {
            Rasterizer::new(self, state).line(a, b, skip_last_pixel);

        } else if let Some(clipping) = state.clipping {
            self.line_clipped(a, b, state, clipping, skip_last_pixel);

        } else {
            self.line_clipped(a, b, state, &[
                Vec2::new(0.0, 0.0),
                Vec2::new(self.size.x as f32, 0.0),
                Vec2::new(self.size.x as f32, self.size.y as f32),
                Vec2::new(0.0, self.size.y as f32)
            ], skip_last_pixel);
        }
    }

    pub fn tri(&mut self, mut a: Vec2UV, mut b: Vec2UV, mut c: Vec2UV, state: &CanvasState) {
        // Tri's apply the rounding inside the edge calculation in order to produce correct UVs
        a.pos = state.transform.transform_point2(a.pos);
        b.pos = state.transform.transform_point2(b.pos);
        c.pos = state.transform.transform_point2(c.pos);

        if self.in_bounds(a.pos) && self.in_bounds(b.pos) && self.in_bounds(c.pos) {
            Rasterizer::new(self, state).triangle(a, b, c);

        } else if let Some(clipping) = state.clipping {
            self.tri_clipped(a, b, c, state, clipping);

        } else {
            self.tri_clipped(a, b, c, state, &[
                Vec2::new(0.0, 0.0),
                Vec2::new(self.size.x as f32, 0.0),
                Vec2::new(self.size.x as f32, self.size.y as f32),
                Vec2::new(0.0, self.size.y as f32)
            ]);
        }
    }

    fn line_clipped(&mut self, a: Vec2, b: Vec2, state: &CanvasState, clipping: &[Vec2], skip_last_pixel: bool) {
        if let Some((a, b)) = Clip::line(a, b, clipping) {
            Rasterizer::new(self, state).line(a, b, skip_last_pixel);
        }
    }

    fn tri_clipped(&mut self, a: Vec2UV, b: Vec2UV, c: Vec2UV, state: &CanvasState, clipping: &[Vec2]) {
        Clip::polygon(a, b, c, clipping, |a, b, c| {
            Rasterizer::new(self, state).triangle(a, b, c);
        });
    }

    fn in_bounds(&self, a: Vec2) -> bool {
        let x = f32_to_i32(a.x);
        let y = f32_to_i32(a.y);
        x >= 0 && y >= 0 && x < self.size.x as i32 && y < self.size.y as i32
    }
}

