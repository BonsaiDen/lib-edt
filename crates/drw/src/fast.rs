// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::Color;


// Unsafe Helpers to speed up low level pixel plotting routines ---------------
// ----------------------------------------------------------------------------
#[inline(always)]
pub fn read_argb32_no_bounds(buffer: &[u32], index: usize) -> Color {
    unsafe { *buffer.get_unchecked(index) }.into()
}

#[inline(always)]
pub fn write_argb32_no_bounds(buffer: &mut [u32], index: usize, color: Color) {
    unsafe {
        *buffer.get_unchecked_mut(index) = color.into();
    }
}

#[inline(always)]
pub fn read_rgba8_no_bounds(buffer: &[u8], index: usize) -> Color {
    let offset = index * 4;
    let r = unsafe { *buffer.get_unchecked(offset) } as u8;
    let g = unsafe { *buffer.get_unchecked(offset + 1) } as u8;
    let b = unsafe { *buffer.get_unchecked(offset + 2) } as u8;
    let a = unsafe { *buffer.get_unchecked(offset + 3) } as u8;
    [r, g, b, a].into()
}

#[inline(always)]
pub fn write_rgba8_no_bounds(buffer: &mut [u8], index: usize, color: Color) {
    let offset = index * 4;
    let bytes: [u8; 4] = color.into();
    unsafe {
        buffer.get_unchecked_mut(offset..offset + 4).copy_from_slice(&bytes)
    }
}

#[inline(always)]
pub fn read_pixel_no_bounds(buffer: &[u8], index: usize) -> u8 {
    unsafe { *buffer.get_unchecked(index) }
}

#[inline(always)]
pub fn f32_to_i32(i: f32) -> i32 {
    unsafe { i.to_int_unchecked() }
}

#[inline(always)]
pub fn f32_to_u32(i: f32) -> u32 {
    unsafe { i.to_int_unchecked() }
}

#[inline(always)]
pub fn f32_to_usize(i: f32) -> usize {
    unsafe { i.to_int_unchecked() }
}

#[inline(always)]
pub fn f32_max(a: f32, b: f32) -> f32 {
    // Faster than the optimized version of a.max(b)
    if a > b {
        a

    } else {
        b
    }
}

