// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
const BLEND_AMASK: u32 = 0xFF000000;
const BLEND_RBMASK: u32 = 0x00FF00FF;
const BLEND_GMASK: u32 = 0x0000FF00;
const BLEND_AGMASK: u32 = BLEND_AMASK | BLEND_GMASK;
const BLEND_ONEALPHA: u32 = 0x01000000;


// Color Abstraction ----------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Debug, Clone, Copy)]
pub struct Color {
    color: [u8; 4]
}

impl Color {
    pub const BLACK: Color = Color::rgb(0, 0, 0);
    pub const WHITE: Color = Color::rgb(255, 255, 255);

    pub const RED: Color = Color::rgb(255, 0, 0);
    pub const YELLOW: Color = Color::rgb(255, 255, 0);
    pub const GREEN: Color = Color::rgb(0, 255, 0);
    pub const TEAL: Color = Color::rgb(0, 255, 255);
    pub const BLUE: Color = Color::rgb(0, 0, 255);
    pub const PINK: Color = Color::rgb(255, 0, 255);

    pub const fn rgb(r: u8, g: u8, b: u8) -> Self {
        Self {
            color: [r, g, b, 255]
        }
    }

    pub const fn rgba(r: u8, g: u8, b: u8, a: u8) -> Self {
        Self {
            color: [r, g, b, a]
        }
    }

    #[inline(always)]
    pub fn r(&self) -> u8 {
        self.color[0]
    }

    #[inline(always)]
    pub fn g(&self) -> u8 {
        self.color[1]
    }

    #[inline(always)]
    pub fn b(&self) -> u8 {
        self.color[2]
    }

    #[inline(always)]
    pub fn a(&self) -> u8 {
        self.color[3]
    }

    #[inline(always)]
    pub fn with_alpha(mut self, a: u8) -> Self {
        self.color[3] = a;
        self
    }

    #[inline(always)]
    pub fn blend_with(self, other: Color) -> Self {
        let a: u32 = self.into();
        let b: u32 = other.into();
        let alpha = (b & BLEND_AMASK) >> 24;
        let na = 255 - alpha;
        let rb = ((na * (a & BLEND_RBMASK)) + (alpha * (b & BLEND_RBMASK))) >> 8;
        let ag = (na * ((a & BLEND_AGMASK) >> 8)) + (alpha * (BLEND_ONEALPHA | ((b & BLEND_GMASK) >> 8)));
        ((rb & BLEND_RBMASK) | (ag & BLEND_AGMASK)).into()
    }
}

impl From<Color> for u32 {
    /// Convert the color into a u32 with ARGB layout.
    fn from(c: Color) -> u32 {
        (c.color[3] as u32) << 24 | ((c.color[0] as u32) << 16) | ((c.color[1] as u32) << 8) | c.color[2] as u32
    }
}

impl From<u32> for Color {
    /// Convert the u32 with ARGB layout into a Color.
    fn from(c: u32) -> Self {
        Self {
            color: [(c >> 16) as u8, (c >> 8) as u8, c as u8, (c >> 24) as u8]
        }
    }
}

impl From<(u8, u8, u8)> for Color {
    fn from((r, g, b): (u8, u8, u8)) -> Self {
        Self {
            color: [r, g, b, 255]
        }
    }
}

impl From<(u8, u8, u8, u8)> for Color {
    fn from((r, g, b, a): (u8, u8, u8, u8)) -> Self {
        Self {
            color: [r, g, b, a]
        }
    }
}

impl From<[u8; 3]> for Color {
    fn from(c: [u8; 3]) -> Self {
        Self {
            color: [c[0], c[1], c[2], 255]
        }
    }
}

impl From<[u8; 4]> for Color {
    fn from(color: [u8; 4]) -> Self {
        Self {
            color
        }
    }
}

impl From<Color> for [u8; 4] {
    fn from(color: Color) -> [u8; 4] {
        color.color
    }
}

