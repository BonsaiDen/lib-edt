// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::Vec2UV;
use crate::fast::{f32_to_u32, f32_max};
use crate::rasterizer::PIXEL_ROUNDING;
use super::gradient::{Gradient, GradientValue};


// Edge -----------------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub struct EdgeData {
    y_start: u32,
    y_end: u32,
    y_prestep: f32,
    x: f32,
    x_step: f32,
    x_prestep: f32,
    y_dist: f32,
}

impl EdgeData {
    fn new(min_y: Vec2UV, max_y: Vec2UV) -> Self {
        // Rounding is added here for correct UVs
        let y_start = f32_to_u32(min_y.pos.y + PIXEL_ROUNDING);
        let y_end = f32_to_u32(max_y.pos.y + PIXEL_ROUNDING);
        let y_dist = f32_max(max_y.pos.y - min_y.pos.y, 0.5);
        let y_prestep = y_start as f32 - min_y.pos.y + PIXEL_ROUNDING;

        let x_dist = max_y.pos.x - min_y.pos.x;
        let x_step = x_dist / y_dist;
        let x = min_y.pos.x + y_prestep * x_step;
        let x_prestep = x - min_y.pos.x + PIXEL_ROUNDING * 2.0;
        Self {
            x: x + PIXEL_ROUNDING,
            x_step,
            x_prestep,
            y_start,
            y_end,
            y_dist: (y_end - y_start) as f32,
            y_prestep
        }
    }
}

#[derive(Clone)]
pub struct Edge {
    data: EdgeData,
    pub u: EdgeValue,
    pub v: EdgeValue
}

impl Edge {
    pub fn from_a(gradient: &Gradient, min_y: Vec2UV, max_y: Vec2UV) -> Self {
        let data = EdgeData::new(min_y, max_y);
        Self {
            u: EdgeValue::from_gradient_a(&gradient.u, data.x_step, data.x_prestep, data.y_prestep),
            v: EdgeValue::from_gradient_a(&gradient.v, data.x_step, data.x_prestep, data.y_prestep),
            data
        }
    }

    pub fn from_b(gradient: &Gradient, min_y: Vec2UV, max_y: Vec2UV) -> Self {
        let data = EdgeData::new(min_y, max_y);
        Self {
            u: EdgeValue::from_gradient_b(&gradient.u, data.x_step, data.x_prestep, data.y_prestep),
            v: EdgeValue::from_gradient_b(&gradient.v, data.x_step, data.x_prestep, data.y_prestep),
            data
        }
    }

    pub fn y_start(&self) -> u32 {
        self.data.y_start
    }

    pub fn y_end(&self) -> u32 {
        self.data.y_end
    }

    pub fn y_dist(&self) -> f32 {
        self.data.y_dist
    }

    pub fn x(&self) -> f32 {
        self.data.x
    }

    pub fn lower_half(&self, offset: f32) -> Self {
        let mut e = self.clone();
        e.data.x += self.data.x_step * offset;
        e.u.offset_by(offset);
        e.v.offset_by(offset);
        e
    }

    pub fn step(&mut self) {
        self.data.x += self.data.x_step;
        self.u.step();
        self.v.step();
    }
}

#[derive(Debug, Clone)]
pub struct EdgeValue {
    pub value: f32,
    step: f32
}

impl EdgeValue {
    pub fn from_gradient_a(gradient: &GradientValue, x_step: f32, x_prestep: f32, y_prestep: f32) -> Self {
        Self {
            value: gradient.a + gradient.x_step * x_prestep + gradient.y_step * y_prestep,
            step: gradient.y_step + gradient.x_step * x_step
        }
    }

    pub fn from_gradient_b(gradient: &GradientValue, x_step: f32, x_prestep: f32, y_prestep: f32) -> Self {
        Self {
            value: gradient.b + gradient.x_step * x_prestep + gradient.y_step * y_prestep,
            step: gradient.y_step + gradient.x_step * x_step
        }
    }

    pub fn offset_by(&mut self, offset: f32) {
        self.value += self.step * offset;
    }

    pub fn step(&mut self) {
        self.value += self.step;
    }
}

