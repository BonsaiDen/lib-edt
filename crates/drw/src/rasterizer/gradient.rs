// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::Vec2UV;
use super::edge::Edge;


// Gradient -------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct Gradient {
    pub u: GradientValue,
    pub v: GradientValue
}

impl Gradient {
    pub fn new(min_y: Vec2UV, mid_y: Vec2UV, max_y: Vec2UV) -> Self {
        let q = [
            mid_y.pos.x - max_y.pos.x,
            min_y.pos.y - max_y.pos.y,
            min_y.pos.x - max_y.pos.x,
            mid_y.pos.y - max_y.pos.y
        ];

        let one_over_dx = 1.0 / (q[0] * q[1] - q[2] * q[3]);
        let one_over_dy = -one_over_dx;
        Self {
           u: GradientValue::new(
               [min_y.uv.x, mid_y.uv.x, max_y.uv.x], q, one_over_dx, one_over_dy
           ),
           v: GradientValue::new(
               [min_y.uv.y, mid_y.uv.y, max_y.uv.y], q, one_over_dx, one_over_dy
           )
        }
    }

    pub fn edges(&self, min_y: Vec2UV, mid_y: Vec2UV, max_y: Vec2UV) -> (Edge, Edge, Edge, Edge) {
        let top_to_bottom = Edge::from_a(self, min_y, max_y);
        let top_to_middle = Edge::from_a(self, min_y, mid_y);
        let top_from_middle = top_to_bottom.lower_half(top_to_middle.y_dist());
        let middle_to_bottom = Edge::from_b(self, mid_y, max_y);
        (top_to_bottom, top_to_middle, top_from_middle, middle_to_bottom)
    }
}

#[derive(Clone)]
pub struct GradientValue {
    pub a: f32,
    pub b: f32,
    pub x_step: f32,
    pub y_step: f32
}

impl GradientValue {
    pub fn new(values: [f32; 3], q: [f32; 4], one_over_dx: f32, one_over_dy: f32)  -> Self {
        let a = values[1] - values[2];
        let b = values[0] - values[2];
        Self {
            a: values[0],
            b: values[1],
            x_step: (a * q[1] - b * q[3]) * one_over_dx,
            y_step: (a * q[2] - b * q[0]) * one_over_dy
        }
    }
}

