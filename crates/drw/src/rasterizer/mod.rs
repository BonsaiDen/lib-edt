// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::marker::PhantomData;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use glam::Vec2;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
pub mod clip;
mod edge;
mod gradient;
mod interpolator;

use self::edge::Edge;
use self::gradient::Gradient;
use self::interpolator::Interpolator;
use crate::{BlendMode, CanvasBuffer, CanvasState, PxlType};
use crate::fast::{f32_to_u32, f32_to_i32};


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub const PIXEL_ROUNDING: f32 = 0.5;


// Types ----------------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Debug, Clone, Copy)]
pub struct Vec2UV {
    pub pos: Vec2,
    pub uv: Vec2
}

impl Vec2UV {
    pub fn new(pos: Vec2, uv: Vec2) -> Self {
        Self {
            pos,
            uv
        }
    }

    pub(crate) fn lerp(&self, o: Vec2UV, t: f32) -> Self {
        Self {
            pos: self.pos.lerp(o.pos, t),
            uv: self.uv.lerp(o.uv, t)
        }
    }

    pub(crate) fn triangle_area_times_two(&self, b: Vec2UV, c: Vec2UV) -> f32 {
        let x1 = b.pos.x - self.pos.x;
        let y1 = b.pos.y - self.pos.y;
        let x2 = c.pos.x - self.pos.x;
        let y2 = c.pos.y - self.pos.y;
        x1 * y2 - x2 * y1
    }
}


// Low Level Rasterzation -----------------------------------------------------
// ----------------------------------------------------------------------------
pub(crate) struct Rasterizer<'a, 'b, 'c, T: PxlType> {
    buffer: &'a mut CanvasBuffer<'c, T>,
    state: &'a CanvasState<'b>,
    depth: PhantomData<T>
}

impl<'a, 'b, 'c, T: PxlType> Rasterizer<'a, 'b, 'c, T> {
    #[inline(always)]
    pub fn new(buffer: &'a mut CanvasBuffer<'c, T>, state: &'a CanvasState<'b>) -> Self {
        Self {
            buffer,
            state,
            depth: PhantomData
        }
    }

    #[inline(always)]
    pub fn pixel(&mut self, x: u32, y: u32) {
        let i = (x + y * self.buffer.width()) as usize;
        self.buffer.write_color(i, self.state.color);
    }

    pub fn line(&mut self, a: Vec2, b: Vec2, skip_last_pixel: bool) {
        let width = self.buffer.width() as i32;
        let height = self.buffer.height() as i32;
        let mut x = f32_to_i32(a.x);
        let mut y = f32_to_i32(a.y);
        let (sign_x, len_x) = sabs(f32_to_i32(b.x), x);
        let (sign_y, len_y) = sabs(f32_to_i32(b.y), y);
        let longest = len_x.max(len_y);
        let mut err_x = longest / 2;
        let mut err_y = longest / 2;
        for _ in 0..longest {
            if x < width && y < height {
                self.buffer.write_color(x as usize + y as usize * width as usize, self.state.color);
            }

            err_x -= len_x;
            if err_x < 0 {
                err_x += longest;
                x += sign_x;
            }

            err_y -= len_y;
            if err_y < 0 {
                err_y += longest;
                y += sign_y;
            }
        }
        if !skip_last_pixel && x < width && y < height {
            self.buffer.write_color(x as usize + y as usize * width as usize, self.state.color);
        }
    }

    pub fn triangle(&mut self, mut min_y: Vec2UV, mut mid_y: Vec2UV, mut max_y: Vec2UV) {
        if max_y.pos.y < mid_y.pos.y {
            std::mem::swap(&mut max_y, &mut mid_y);
        }
        if mid_y.pos.y < min_y.pos.y  {
            std::mem::swap(&mut mid_y, &mut min_y);
        }
        if max_y.pos.y < mid_y.pos.y  {
            std::mem::swap(&mut max_y, &mut mid_y);
        }
        let handedness = min_y.triangle_area_times_two(max_y, mid_y) >= 0.0;
        let gradients = Gradient::new(min_y, mid_y, max_y);
        let (top_to_bottom, top_to_middle, top_from_middle, middle_to_bottom) = gradients.edges(min_y, mid_y, max_y);
        self.edge(&gradients, top_to_bottom, top_to_middle, handedness);
        self.edge(&gradients, top_from_middle, middle_to_bottom, handedness);
    }

    fn edge(&mut self, gradient: &Gradient, mut a: Edge, mut b: Edge, handedness: bool) {
        let y_start = b.y_start();
        let y_end = b.y_end();
        if handedness {
            std::mem::swap(&mut a, &mut b);
        }
        let mut i = Interpolator::create();
        for y in y_start..y_end {
            let x_min = f32_to_u32(a.x());
            let x_max = f32_to_u32(b.x());
            i.line(gradient, &a, x_min as f32 - a.x());
            self.scan_line(y, x_min, x_max, &mut i);
            a.step();
            b.step();
        }
    }

    #[inline(always)]
    fn scan_line(&mut self, screen_y: u32, x_min: u32, x_max: u32, interpolator: &mut Interpolator) {
        let mut i = (x_min + screen_y * self.buffer.width()) as usize;
        for screen_x in x_min..x_max {
            let color = self.state.sampler.map(|s| {
                let u = (interpolator.u() + self.state.uv_offset.x) * self.state.uv_scale.x;
                let v = (interpolator.v() + self.state.uv_offset.y) * self.state.uv_scale.y;
                let u = self.state.uv_mapping.map(u);
                let v = self.state.uv_mapping.map(v);
                s.sample_pixel(screen_x, screen_y, u, v, self.state.color)

            }).unwrap_or(self.state.color);
            match (color.a(), self.state.blend_mode) {
                (0, _) => {},
                (255, _) | (_, BlendMode::None) => {
                    self.buffer.write_color(i, color);
                },
                (_, BlendMode::Alpha) => {
                    self.buffer.write_color(i, self.buffer.read_color(i).blend_with(color));
                }
            }
            interpolator.step();
            i += 1;
        }
    }
}

fn sabs(a: i32, b: i32) -> (i32, i32) {
    if a > b {
        (1, a - b)

    } else {
        (-1, b - a)
    }
}

