// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use super::{edge::Edge, gradient::Gradient};


// Gradient -------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct Interpolator {
    u: f32,
    u_step: f32,
    v: f32,
    v_step: f32
}

impl Interpolator {
    pub fn create() -> Self {
        Self {
            u: 0.0,
            u_step: 0.0,
            v: 0.0,
            v_step: 0.0,
        }
    }

    pub fn u(&self) -> f32 {
        self.u
    }

    pub fn v(&self) -> f32 {
        self.v
    }

    pub fn line(&mut self, gradient: &Gradient, left: &Edge, x_prestep: f32) {
        self.u = left.u.value + gradient.u.x_step * x_prestep;
        self.v = left.v.value + gradient.v.x_step * x_prestep;
        self.u_step = gradient.u.x_step;
        self.v_step = gradient.v.x_step;
    }

    pub fn step(&mut self) {
        self.u += self.u_step;
        self.v += self.v_step;
    }
}

