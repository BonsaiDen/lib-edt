// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::f32::consts::PI;


// Enums ----------------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Debug, Copy, Clone)]
pub enum BlendMode {
    None,
    Alpha
}

#[derive(Debug, Copy, Clone)]
pub enum UVMapping {
    Clamp,
    Repeat
}

impl UVMapping {
    #[inline(always)]
    pub(crate) fn map(&self, v: f32) -> f32 {
        match self {
            Self::Clamp => v.clamp(0.0, 1.0),
            Self::Repeat => v % 1.0
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum CircleResolution {
    SegmentCount(usize),
    SegmentLength(f32)
}

#[derive(Debug, Clone, Copy)]
pub enum Rotation {
    Deg(f32),
    Rad(f32)
}

impl From<Rotation> for f32 {
    fn from(r: Rotation) -> Self {
        r.to_rad()
    }
}

impl Rotation {
    pub fn to_deg(self) -> f32 {
        match self {
            Self::Deg(d) => d,
            Self::Rad(r) => r * 180.0 / PI
        }
    }

    pub fn to_rad(&self) -> f32 {
        match self {
            Self::Deg(d) => *d * PI / 180.0,
            Self::Rad(r) => *r
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub enum Align {
    Start,
    Middle,
    End,
}

impl Align {
    pub(crate) fn apply_rect(&self, position: f32, half_size: f32) -> f32 {
        match self {
            Self::Start => position + half_size,
            Self::Middle => position,
            Self::End => position - half_size,
        }
    }

    pub(crate) fn apply_text(&self, position: i32, size: i32) -> i32 {
        match self {
            Self::Start => position,
            Self::Middle => position - size / 2,
            Self::End => position - size,
        }
    }
}

