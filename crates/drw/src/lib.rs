// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::ops::Deref;
use std::ops::DerefMut;
use std::f32::consts::PI;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use glam::{Affine2, Vec2, UVec2};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
mod buffer;
mod color;
mod enums;
mod fast;
mod font;
mod image;
mod rasterizer;
use self::buffer::CanvasBuffer;


// Re-Exports -----------------------------------------------------------------
// ----------------------------------------------------------------------------
pub use self::buffer::PxlType;
pub use self::color::Color;
pub use self::enums::*;
pub use self::font::PxlFont;
pub use self::image::{PxlImage, PxlImageBuffer};
pub use self::rasterizer::Vec2UV;


// Traits ---------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub trait PxlCanvasBuffer<'a> {
    type Pixel: PxlType;
    fn pxl_canvas<S: Into<UVec2>>(&'a mut self, size: S) -> PxlCanvas<'a, Self::Pixel> where Self: Sized;
}

impl<'a, T: PxlType> PxlCanvasBuffer<'a> for &mut [T] {
    type Pixel = T;
    fn pxl_canvas<S: Into<UVec2>>(&'a mut self, size: S) -> PxlCanvas<'a, Self::Pixel> {
        let size = size.into();
        assert!(self.len() == (size.x * size.y) as usize * T::ELEMENTS, "invalid pixel buffer size");
        PxlCanvas::from_buffer(self, size)
    }
}

pub trait PixelSampler {
    fn sample_pixel(&self, screen_x: u32, screen_y: u32, u: f32, v: f32, color: Color) -> Color;
}

pub trait UVs {
    fn with_uvs<U: Into<Vec2>>(self, uv: U) -> Vec2UV;
}

impl UVs for Vec2 {
    fn with_uvs<U: Into<Vec2>>(self, uv: U) -> Vec2UV {
        Vec2UV {
            pos: self,
            uv: uv.into()
        }
    }
}


// 2D Pixel Canvas ------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Clone)]
struct CanvasState<'a> {
    color: Color,
    fill: bool,
    line_width: Option<f32>,
    transform: Affine2,
    clipping: Option<&'a [Vec2]>,
    blend_mode: BlendMode,
    uv_mapping: UVMapping,
    uv_scale: Vec2,
    uv_offset: Vec2,
    align: (Align, Align),
    sampler: Option<&'a dyn PixelSampler>,
}

pub struct PxlCanvas<'a, T: PxlType> {
    buffer: CanvasBuffer<'a, T>,
    stack: Vec<CanvasState<'a>>,
    state: CanvasState<'a>
}

impl<'a, T: PxlType> PxlCanvas<'a, T> {
    pub fn from_buffer<S: Into<UVec2>>(pixels: &'a mut [T], size: S) -> Self {
        let size = size.into();
        Self {
            stack: vec![],
            state: CanvasState {
                color: Color::WHITE,
                fill: false,
                line_width: None,
                transform: Affine2::IDENTITY,
                clipping: None,
                blend_mode: BlendMode::None,
                uv_mapping: UVMapping::Clamp,
                uv_scale: Vec2::new(1.0, 1.0),
                uv_offset: Vec2::ZERO,
                align: (Align::Middle, Align::Middle),
                sampler: None,
            },
            buffer: CanvasBuffer::new(pixels, size)
        }
    }
}

impl<'a, T: PxlType> PxlCanvas<'a, T> {
    pub fn push(&mut self) -> &mut Self {
        self.stack.push(self.state.clone());
        self
    }

    pub fn pop(&mut self) -> &mut Self {
        if let Some(state) = self.stack.pop() {
            self.state = state;
        }
        self
    }

    pub fn blend(&mut self, mode: BlendMode) -> &mut Self {
        self.state.blend_mode = mode;
        self
    }

    pub fn line_width<U: Into<Option<f32>>>(&mut self, width: U) -> &mut Self {
        self.state.line_width = width.into();
        self
    }

    pub fn fill(&mut self, fill: bool) -> &mut Self {
        self.state.fill = fill;
        self
    }

    pub fn sampler(&mut self, sampler: Option<&'a dyn PixelSampler>) -> &mut Self {
        self.state.sampler = sampler;
        self
    }

    pub fn clipping(&mut self, clipping: &'a [Vec2]) -> &mut Self {
        self.state.clipping = Some(clipping);
        self
    }

    pub fn color<C: Into<Color>>(&mut self, color: C) -> &mut Self {
        self.state.color = color.into();
        self
    }

    pub fn uv_mapping(&mut self, mapping: UVMapping) -> &mut Self {
        self.state.uv_mapping = mapping;
        self
    }

    pub fn uv_offset<V: Into<Vec2>>(&mut self, offset: V) -> &mut Self {
        self.state.uv_offset = offset.into();
        self
    }

    pub fn uv_scale<V: Into<Vec2>>(&mut self, scale: V) -> &mut Self {
        self.state.uv_scale = scale.into();
        self
    }

    pub fn align(&mut self, halign: Align, valign: Align) -> &mut Self {
        self.state.align = (halign, valign);
        self
    }

    pub fn translate<V: Into<Vec2>>(&mut self, translation: V) -> &mut Self {
        self.state.transform = self.state.transform * Affine2::from_translation(translation.into());
        self
    }

    pub fn rotate(&mut self, rotation: Rotation) -> &mut Self {
        self.state.transform = self.state.transform * Affine2::from_angle(rotation.to_rad());
        self
    }

    pub fn scale<V: Into<Vec2>>(&mut self, scale: V) -> &mut Self {
        self.state.transform = self.state.transform * Affine2::from_scale(scale.into());
        self
    }

    pub fn with_font(&'a mut self, font: &'a PxlFont, size: f32) -> PxlFontCanvas<'a, T> {
        PxlFontCanvas {
            draw: self,
            size,
            font: Some(font),
        }
    }

    pub fn clear(&mut self) -> &mut Self {
        self.buffer.clear(self.state.color);
        self
    }

    /// Draws a line between the specified points.
    pub fn line<V: Into<Vec2>>(&mut self, start: V, end: V) -> &mut Self {
        if let Some(w) = self.state.line_width {
            let s = start.into();
            let e = end.into();
            let w = w * 0.5;
            let r = (e.y - s.y).atan2(e.x - s.x) + PI * 0.5;
            let q = r + PI;
            let a = s + Vec2::new(r.cos() * w, r.sin() * w);
            let b = s + Vec2::new(q.cos() * w, q.sin() * w);
            let c = e + Vec2::new(q.cos() * w, q.sin() * w);
            let d = e + Vec2::new(r.cos() * w, r.sin() * w);
            self.quad(a, b, c, d)

        } else {
            self.buffer.line(start.into(), end.into(), &self.state, false);
            self
        }
    }

    /// Draws a circle at the specified point with the given radius and resolution.
    pub fn circle<V: Into<Vec2>>(
        &mut self,
        center: V,
        radius: f32,
        resolution: CircleResolution

    ) -> &mut Self {
        let circ = radius * PI;
        let segments = match resolution {
            CircleResolution::SegmentCount(count) => count as f32,
            CircleResolution::SegmentLength(length) => (circ / length).floor(),
        };
        let arc_radians = PI * 2.0 / segments;
        let mut state = self.state.clone();
        state.color = (255, 0, 0).into();

        let count = segments as usize;
        let center = center.into();
        let mut last = center + Vec2::new(radius, 0.0);
        let mut last_uv = Vec2::new(1.0, 0.0);
        for s in 0..count + 1 {
            let r = s as f32 * arc_radians;
            let next = center + Vec2::new(r.cos() * radius, r.sin() * radius);
            let next_uv = Vec2::new(r.cos() * 0.5 + 0.5, r.sin() * 0.5 + 0.5);
            if self.state.fill {
                self.tri(last.with_uvs(last_uv), next.with_uvs(next_uv), center.with_uvs(Vec2::new(0.5, 0.5)));

            } else {
                self.buffer.line(last, next, &self.state, true);
            }
            last = next;
            last_uv = next_uv;
        }
        self
    }

    /// Draws a rectangle at the specified point using the given extends.
    pub fn rect<V: Into<Vec2>>(&mut self, center: V, extends: V) -> &mut Self {
        let mut p = center.into();
        let s = extends.into();
        p.x = self.state.align.0.apply_rect(p.x, s.x);
        p.y = self.state.align.1.apply_rect(p.y, s.y);
        if self.state.fill {
            let a = p + Vec2::new(-s.x, -s.y);
            let b = p + Vec2::new(s.x, -s.y);
            let c = p + Vec2::new(s.x, s.y);
            let d = p + Vec2::new(-s.x, s.y);
            self.quad(a, b, c, d)

        } else {
            let a = p + Vec2::new(-s.x, -s.y);
            let b = p + Vec2::new(s.x - 1.0, -s.y);
            let c = p + Vec2::new(s.x - 1.0, s.y - 1.0);
            let d = p + Vec2::new(-s.x, s.y - 1.0);
            self.quad(a, b, c, d)
        }
    }

    /// Draw an image at the specified point.
    pub fn image<V: Into<Vec2>>(&mut self, p: V, image: &'a PxlImage) -> &mut Self {
        self.push();
        self.sampler(Some(image));
        self.fill(true);
        self.rect(p.into(), image.size() * 0.5);
        self.pop();
        self
    }

    /// Draws a single pixel at the specified point.
    pub fn pixel<V: Into<Vec2>>(&mut self, center: V) -> &mut Self {
        self.buffer.pixel(center.into(), &self.state);
        self
    }

    /// Draws a triangle for the specified points and UVs.
    pub fn tri(&mut self, a: Vec2UV, b: Vec2UV, c: Vec2UV) -> &mut Self {
        if self.state.fill {
            self.buffer.tri(a, b, c, &self.state);

        } else {
            self.buffer.line(a.pos, b.pos, &self.state, true);
            self.buffer.line(b.pos, c.pos, &self.state, true);
            self.buffer.line(c.pos, a.pos, &self.state, true);
        }
        self
    }

    /// Draws a quad for the specified points.
    pub fn quad<V: Into<Vec2>>(&mut self, a: V, b: V, c: V, d: V) -> &mut Self {
        let a = a.into();
        let b = b.into();
        let c = c.into();
        let d = d.into();
        if self.state.fill {
            self.buffer.tri(a.with_uvs((0.0, 0.0)), b.with_uvs((1.0, 0.0)), c.with_uvs((1.0, 1.0)), &self.state);
            self.buffer.tri(a.with_uvs((0.0, 0.0)), c.with_uvs((1.0, 1.0)), d.with_uvs((0.0, 1.0)), &self.state);

        } else {
            self.buffer.line(a, b, &self.state, true);
            self.buffer.line(b, c, &self.state, true);
            self.buffer.line(c, d, &self.state, true);
            self.buffer.line(d, a, &self.state, true);
        }
        self
    }
}

pub struct PxlFontCanvas<'a, T: PxlType> {
    draw: &'a mut PxlCanvas<'a, T>,
    size: f32,
    font: Option<&'a PxlFont>
}

impl<'a, T: PxlType> Deref for PxlFontCanvas<'a, T> {
    type Target = PxlCanvas<'a, T>;
    fn deref(&self) -> &Self::Target {
        self.draw
    }
}

impl<'a, T: PxlType> DerefMut for PxlFontCanvas<'a, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.draw
    }
}

impl<'a, T: PxlType> PxlFontCanvas<'a, T> {
    pub fn size(&mut self, size: f32) -> &mut Self {
        self.size = size;
        self
    }

    pub fn text<V: Into<Vec2>>(&mut self, center: V, text: &str) -> &mut Self {
        let p = center.into();
        self.push();
        self.fill(true);
        self.font.unwrap().layout(text, self.size, self.state.align, |ox, oy, w, h, glyph| {
            let mut state = self.state.clone();
            state.sampler = Some(&glyph);

            let a = p + Vec2::new(ox as f32, oy as f32);
            let b = p + Vec2::new(ox as f32 + w as f32, oy as f32);
            let c = p + Vec2::new(ox as f32 + w as f32, oy as f32 + h as f32);
            let d = p + Vec2::new(ox as f32, oy as f32 + h as f32);
            self.buffer.tri(Vec2UV::new(a, Vec2::new(0.0, 0.0)), Vec2UV::new(b, Vec2::new(1.0, 0.0)), Vec2UV::new(c, Vec2::new(1.0, 1.0)), &state);
            self.buffer.tri(Vec2UV::new(a, Vec2::new(0.0, 0.0)), Vec2UV::new(c, Vec2::new(1.0, 1.0)), Vec2UV::new(d, Vec2::new(0.0, 1.0)), &state);
        });
        self.pop();
        self
    }
}

