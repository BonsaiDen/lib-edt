// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::error::Error;
use std::path::PathBuf;
use std::marker::PhantomData;
use std::time::{Instant, Duration};


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use spin_sleep::LoopHelper;
use pixels::{PixelsBuilder, SurfaceTexture};

use serde::{Serialize, Deserialize, de::DeserializeOwned};

use winit::dpi::PhysicalPosition;
use winit::event::{Event, WindowEvent};
use winit::window::WindowBuilder;
use winit::platform::run_return::EventLoopExtRunReturn;
use winit::event_loop::{ControlFlow, EventLoop};

pub use winit::dpi::LogicalSize;
pub use winit::window::{Fullscreen, Window};
pub use winit_input_helper::WinitInputHelper as Input;
pub use winit::event::VirtualKeyCode as Key;

use imgui::{Context, FontConfig, FontSource};
use imgui_wgpu::Renderer;
use imgui_winit_support::WinitPlatform;
use pixels::wgpu;


// Modules --------------------------------------------------------------------
// ----------------------------------------------------------------------------
mod context;
pub use self::context::{AppContext, AudioContext, ClipboardSupport, PixelContext, UiTexture, Update, Render};
pub use imgui;
pub use implot;
pub use drw;


// App Types ------------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
struct WindowState {
    #[serde(default)]
    position: Option<(i32, i32)>,
    #[serde(default)]
    size: Option<(u32, u32)>
}

#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
struct ApplicationState<T: AppState> {
    window: WindowState,
    #[serde(bound(deserialize = ""))]
    inner: T
}

pub trait AppCommand {
    fn is_exit(&self) -> bool;
}

impl AppCommand for () {
    fn is_exit(&self) -> bool {
        true
    }
}

pub trait AppState: Default + std::fmt::Debug + Clone + PartialEq + Eq + Serialize + DeserializeOwned {
    type Command: AppCommand;
    fn filename() -> Option<PathBuf> {
        Some(".app.toml".into())
    }
}

impl AppState for () {
    type Command = ();
    fn filename() -> Option<PathBuf> {
        None
    }
}

#[derive(Debug, Copy, Clone)]
pub enum RenderStyle {
    Pixels(u32, u32),
    Background([f32; 4])
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum RenderMode {
    /// Call `App::render` once for and directly after each `App::update` call.
    ///
    /// This has the smallest possible latency, but migh increase power usage.
    Immediate,

    /// Let the compositor schedule `App::render` calls.
    ///
    /// This has higher latency (at least one frame) but might reduce power usage.
    Composite
}

#[derive(Debug, Copy, Clone)]
pub struct AppConfig {
    pub window_title: &'static str,
    pub window_size: (u32, u32, bool),
    pub frame_rate: Option<u8>,
    pub resizeable: bool,
    pub style: RenderStyle,
    pub mode: RenderMode
}

impl Default for AppConfig {
    fn default() -> Self {
        Self {
            window_title: "App",
            window_size: (640, 480, true),
            frame_rate: Some(60),
            resizeable: false,
            style: RenderStyle::Background([0.0, 0.0, 0.0, 1.0]),
            mode: RenderMode::Composite
        }
    }
}

// App Trait ------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub trait App<T: 'static + AppState> {
    fn defaults(ctx: &T) -> AppConfig;

    fn init(&mut self, _ctx: AppContext<Update, T>) {}

    fn update(&mut self, _ctx: AppContext<Update, T>) {}

    fn render(&mut self,  _ctx: AppContext<Render, T>, ui: &imgui::Ui);

    fn command(&mut self, _ctx: AppContext<Update, T>, _command: T::Command) {}

    fn close_request(&mut self) -> bool {
        true
    }

    fn run(mut self) -> Result<(), Box<dyn std::error::Error>> where Self: 'static + Sized {
        let mut event_loop = EventLoop::new();
        let mut runner = AppRunner::<T>::from_event_loop::<Self>(&event_loop)?;
        runner.init(&mut self);

        let wid = runner.window.id();
        event_loop.run_return(move |event, _, control_flow| {
            // Improve timing by always running the closure instead of waiting to be triggerd by a new event
            *control_flow = ControlFlow::Poll;
            match event {
                Event::RedrawRequested(_) if runner.mode == RenderMode::Composite => {
                    // Delayed mode
                    if runner.render(&mut self).is_err() {
                        *control_flow = ControlFlow::Exit;
                    }
                },

                // Only react to the closing of the specific window created previously
                Event::WindowEvent { event: WindowEvent::CloseRequested | WindowEvent::Destroyed, window_id } => if window_id == wid && self.close_request() {
                    *control_flow = ControlFlow::Exit;
                },
                // Frame completed run update logic and limit fps
                Event::MainEventsCleared => {
                    if runner.update(&mut self) {
                        // Immediate mode
                        if runner.mode == RenderMode::Immediate && runner.render(&mut self).is_err() {
                            *control_flow = ControlFlow::Exit;
                        }
                        runner.limit();

                    } else {
                        *control_flow = ControlFlow::Exit;
                    }
                },
                // Handle input and resize events
                event => {
                    runner.event(event);
                }
            }
        });
        Ok(())
    }
}


// Helpers --------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub fn rgba(color: [f32; 4]) -> [f32; 4] {
    const GAMMA: f32 = 2.2;
    let x = color[0].powf(GAMMA);
    let y = color[1].powf(GAMMA);
    let z = color[2].powf(GAMMA);
    let w = 1.0 - (1.0 - color[3]).powf(GAMMA);
    [x, y, z, w]
}


// App Runner -----------------------------------------------------------------
// ----------------------------------------------------------------------------
enum FrameLimiter {
    Limit(LoopHelper),
    ReportOnly(LoopHelper)
}

struct AppRunner<T: 'static + AppState> {
    // Timing
    dt: Duration,
    fps: f32,
    limiter: FrameLimiter,

    // Input
    window: Window,
    clipboard: Option<ClipboardSupport>,
    input: winit_input_helper::WinitInputHelper,
    commands: Vec<T::Command>,

    // Audio
    audio: AudioContext,

    // Rendering
    pixels: PixelContext,
    imgui: Context,
    platform: WinitPlatform,
    renderer: Renderer,
    style: RenderStyle,
    mode: RenderMode,

    // State
    state: ApplicationState<T>,
    state_saved: ApplicationState<T>,
    state_timer: Instant
}

impl<T: 'static + AppState> AppRunner<T> {
    fn from_event_loop<A: App<T>>(event_loop: &EventLoop<()>) -> Result<Self, Box<dyn Error>> {
        let state = Self::load_app_state_toml().unwrap_or_else(|_| ApplicationState::<T>::default());
        let state_saved = ApplicationState::<T>::default();

        let config = A::defaults(&state.inner);
        let (window, pixels) = Self::create_window_and_pixels(event_loop, &config, state.window.position, state.window.size)?;
        let (imgui, platform, renderer) = Self::setup_imgui(&window, &pixels);

        Ok(Self {
            dt: Duration::from_millis(0),
            fps: 0.0,
            limiter: match config.frame_rate {
                Some(f) => FrameLimiter::Limit(LoopHelper::builder().report_interval_s(0.1).build_with_target_rate(f)),
                None => FrameLimiter::ReportOnly(LoopHelper::builder().report_interval_s(0.1).build_with_target_rate(60)),
            },

            window,
            clipboard: ClipboardSupport::init(),
            input: Input::new(),
            commands: Vec::with_capacity(32),

            audio: AudioContext::new(8192),

            pixels,
            imgui,
            platform,
            renderer,
            style: config.style,
            mode: config.mode,

            state,
            state_saved,
            state_timer: Instant::now(),
        })
    }

    fn init<A: App<T>>(&mut self, app: &mut A) {
        app.init(AppContext {
            t: PhantomData,
            state: &mut self.state.inner,
            pixels: &mut self.pixels,
            window: &self.window,
            audio: &mut self.audio,
            clipboard: self.clipboard.as_mut(),
            input: &self.input,
            commands: &mut self.commands,
            dt: self.dt,
            fps: self.fps,
            screen: (0.0, 0.0, 0.0),
            renderer: &mut self.renderer
        });
    }

    fn event(&mut self, event: Event<()>) {
        self.platform.handle_event(self.imgui.io_mut(), &self.window, &event);
        self.input.update(&event);
    }

    fn update<A: App<T>>(&mut self, app: &mut A) -> bool {
        // Keep pixels surface in sync with window dimensions
        if let Some(size) = self.input.window_resized() {
            self.pixels.inner.resize_surface(size.width, size.height);
        }

        // Calculate mouse position relative to pixel buffer
        self.pixels.mouse = if let Some((x, y)) = self.input.mouse() {
            let s = self.window.scale_factor();
            let cursor_position: winit::dpi::LogicalPosition<f32> = winit::dpi::LogicalPosition::new(x, y);
            let cursor_position = cursor_position.to_physical::<f32>(s).into();
            self.pixels.inner.window_pos_to_pixel(cursor_position).ok().map(|(x, y)| (x as i32, y as i32))

        } else {
            None
        };

        // Run Application update logic
        app.update(AppContext {
            t: PhantomData,
            state: &mut self.state.inner,
            pixels: &mut self.pixels,
            window: &self.window,
            audio: &mut self.audio,
            clipboard: self.clipboard.as_mut(),
            input: &self.input,
            commands: &mut self.commands,
            dt: self.dt,
            fps: self.fps,
            screen: (0.0, 0.0, 0.0),
            renderer: &mut self.renderer
        });

        // Command Handling
        let mut running = true;
        let mut commands = Vec::with_capacity(16);
        for command in self.commands.drain(0..) {
            if command.is_exit() {
                running = false;
                break;
            }
            app.command(AppContext {
                t: PhantomData,
                state: &mut self.state.inner,
                pixels: &mut self.pixels,
                window: &self.window,
                audio: &mut self.audio,
                clipboard: self.clipboard.as_mut(),
                input: &self.input,
                commands: &mut commands,
                dt: self.dt,
                fps: self.fps,
                screen: (0.0, 0.0, 0.0),
                renderer: &mut self.renderer

            }, command);
        }
        self.commands = commands;

        if running {

            // Get latest window size and position
            let size = self.window.inner_size();
            self.state.window.size = Some((size.width, size.height));
            if let Ok(p) = self.window.outer_position() {
                self.state.window.position = Some((p.x, p.y));
            }

            // Detect App State changes and automatically save them
            if self.state != self.state_saved && self.state_timer.elapsed() > Duration::from_millis(500) {
                if let Some(filename) = T::filename() {
                    match toml::to_string(&self.state)  {
                        Ok(t) => {
                            std::fs::write(filename, t).ok();
                        },
                        Err(err) => {
                            eprintln!("edt: Failed to serialize AppState: {}", err);
                            eprintln!("{:#?}", self.state);
                        }
                    }
                }
                self.state_saved = self.state.clone();
                self.state_timer = Instant::now();
            }
            true

        } else {
            false
        }
    }

    fn limit(&mut self) {
        // Schedule a redraw from the compositor
        if self.mode == RenderMode::Composite {
            self.window.request_redraw();
        }

        // Limit FPS
        match self.limiter {
            FrameLimiter::Limit(ref mut limiter) => {
                limiter.loop_sleep();
                self.dt = limiter.loop_start();
                if let Some(f) = limiter.report_rate() {
                    self.fps = f as f32;
                }
            },
            FrameLimiter::ReportOnly(ref mut limiter) => {
                self.dt = limiter.loop_start();
                if let Some(f) = limiter.report_rate() {
                    self.fps = f as f32;
                }
            }
        }
    }

    fn render<A: App<T>>(&mut self, app: &mut A) -> Result<(), Box<dyn Error>> {
        // Get window rect of scaled pixel buffer
        let (pixel_scale, rect) = {
            let context = self.pixels.inner.context();
            let scale_factor = self.window.scale_factor();
            let physical_size = self.window.inner_size().to_logical::<f32>(scale_factor);
            let pixel_size = (context.texture_extent.width as f32, context.texture_extent.height as f32);
            (
                (physical_size.width / pixel_size.0).min(physical_size.height / pixel_size.1).max(1.0).floor(),
                context.scaling_renderer.clip_rect()
            )
        };

        // Prepare imgui frame
        let io = self.imgui.io_mut();
        io.update_delta_time(self.dt);
        self.platform.prepare_frame(io, &self.window)?;

        let ui = self.imgui.frame();
        self.platform.prepare_render(&ui, &self.window);

        // Render Application
        app.render(AppContext {
            t: PhantomData,
            state: &mut self.state.inner,
            pixels: &mut self.pixels,
            window: &self.window,
            audio: &mut self.audio,
            clipboard: self.clipboard.as_mut(),
            input: &self.input,
            commands: &mut self.commands,
            dt: self.dt,
            fps: self.fps,
            screen: (rect.0 as f32, rect.1 as f32, pixel_scale),
            renderer: &mut self.renderer

        }, &ui);

        self.pixels.inner.render_with(|encoder, render_target, context| {
            let ops = match self.style {
                RenderStyle::Pixels(_, _) => {
                    // Render pixels buffer
                    context.scaling_renderer.render(encoder, render_target);
                    wgpu::Operations {
                        load: wgpu::LoadOp::Load,
                        store: true,
                    }
                },
                RenderStyle::Background(color) => {
                    wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color {
                            r: color[0] as f64,
                            g: color[1] as f64,
                            b: color[2] as f64,
                            a: color[3] as f64,
                        }),
                        store: true,
                    }
                }
            };

            // Create color pass for imgui rendering
            let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: None,
                color_attachments: &[wgpu::RenderPassColorAttachment {
                    view: render_target,
                    resolve_target: None,
                    ops
                }],
                depth_stencil_attachment: None,
            });

            // Render imgui on top of pixels surface
            self.renderer.render(ui.render(), &context.queue, &context.device, &mut rpass).map_err(|e| e.into())
        })?;
        Ok(())
    }
}

impl<T: 'static + AppState> AppRunner<T> {
    fn load_app_state_toml() -> Result<ApplicationState<T>, Box<dyn Error>> {
        match T::filename() {
            Some(filename) => {
                let t = std::fs::read_to_string(filename)?;
                let s = toml::from_str::<ApplicationState<T>>(&t)?;
                Ok(s)
            },
            None => Ok(ApplicationState::<T>::default())
        }
    }

    fn create_window_and_pixels(event_loop: &EventLoop<()>, config: &AppConfig, position: Option<(i32, i32)>, size: Option<(u32, u32)>) -> Result<(Window, PixelContext), Box<dyn Error>> {
        // Create EventLoop and Window
        let (width, height, is_minimum) = config.window_size;
        let window = {
            let inner_size = LogicalSize::new(width, height);
            let stored_size = if let Some((width, height)) = size {
                LogicalSize::new(width, height)

            } else {
                inner_size
            };
            let mut builder = WindowBuilder::new()
                .with_title(config.window_title)
                .with_inner_size(stored_size);

            if is_minimum {
                builder = builder.with_min_inner_size(inner_size);
            }
            if let Some(position) = position {
                builder = builder.with_position(PhysicalPosition::new(position.0, position.1))
            }
            builder.with_resizable(config.resizeable)
                .build(event_loop)
                .unwrap()
        };

        // Setup Pixels Renderer
        let pixels = {
            let (width, height) = match config.style {
                RenderStyle::Pixels(w, h) => (w, h),
                RenderStyle::Background(_) => (32, 32)
            };
            let window_size = window.inner_size();
            let surface_texture = SurfaceTexture::new(window_size.width, window_size.height, &window);
            let inner = PixelsBuilder::new(width, height, surface_texture)
                .enable_vsync(config.frame_rate.is_none())
                .request_adapter_options(wgpu::RequestAdapterOptions {
                    power_preference: wgpu::PowerPreference::HighPerformance,
                    force_fallback_adapter: false,
                    compatible_surface: None,

                }).build()?;

            PixelContext {
                inner,
                mouse: None,
                width,
                height
            }
        };

        Ok((window, pixels))
    }

    fn setup_imgui(
        window: &Window,
        pixels: &PixelContext

    ) -> (Context, imgui_winit_support::WinitPlatform, imgui_wgpu::Renderer) {
        // Create Dear ImGui context
        let mut imgui = Context::create();
        imgui.set_ini_filename(None);

        // Initialize winit platform support
        let mut platform = imgui_winit_support::WinitPlatform::init(&mut imgui);
        platform.attach_window(
            imgui.io_mut(),
            window,
            imgui_winit_support::HiDpiMode::Default,
        );

        // Configure Fonts
        let hidpi_factor = window.scale_factor().round();
        let font_size = (13.0 * hidpi_factor) as f32 * 2.0;
        imgui.io_mut().font_global_scale = (1.0 / hidpi_factor) as f32 / 2.0;
        imgui.fonts().add_font(&[FontSource::DefaultFontData {
            config: Some(FontConfig {
                oversample_h: 3,
                oversample_v: 3,
                pixel_snap_h: false,
                size_pixels: font_size,
                ..Default::default()
            }),
        }]);

        // Fix incorrect alpha values for sRGB framebuffers
        let style = imgui.style_mut();
        for color in 0..style.colors.len() {
            style.colors[color][3] = rgba(style.colors[color])[3];
        }

        // Fix default table colors
        let c = style.colors[imgui::StyleColor::TableRowBg as usize];
        style.colors[imgui::StyleColor::TableRowBgAlt as usize] = c;

        // Adjust scroll bar colors
        let c = style.colors[imgui::StyleColor::Header as usize];
        style.colors[imgui::StyleColor::ScrollbarGrab as usize] = c;
        let c = style.colors[imgui::StyleColor::HeaderActive as usize];
        style.colors[imgui::StyleColor::ScrollbarGrabActive as usize] = c;
        let c = style.colors[imgui::StyleColor::HeaderHovered as usize];
        style.colors[imgui::StyleColor::ScrollbarGrabHovered as usize] = c;

        // Create WGPU renderer
        let device = pixels.inner.device();
        let queue = pixels.inner.queue();
        let mut config = imgui_wgpu::RendererConfig::new();
        config.texture_format = wgpu::TextureFormat::Bgra8UnormSrgb;
        config.sample_count = 1;

        let renderer = imgui_wgpu::Renderer::new(&mut imgui, device, queue, config);
        (imgui, platform, renderer)
    }
}

