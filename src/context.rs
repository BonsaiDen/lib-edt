// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::ops::Deref;
use std::path::PathBuf;
use std::time::Duration;
use std::sync::{Arc, Mutex};
use std::marker::PhantomData;
use std::sync::atomic::AtomicBool;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crossbeam::queue::ArrayQueue;
use pixels::wgpu::TextureDimension;
use wgpu::{Extent3d, TextureUsages};
use clipboard::{ClipboardContext, ClipboardProvider};
use cpal::{Sample, SampleFormat, SampleRate, Stream};
use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};
use winit_input_helper::WinitInputHelper as Input;
use winit::window::Window;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{AppState, Key};


// Application Context --------------------------------------------------------
// ----------------------------------------------------------------------------
pub trait ContextType {}

pub struct Render;
impl ContextType for Render {}

pub struct Update;
impl ContextType for Update {}

pub struct AppContext<'a, T: ContextType, S: AppState> {
    pub state: &'a mut S,
    pub pixels: &'a mut PixelContext,
    pub window: &'a Window,
    pub audio: &'a mut AudioContext,
    pub dt: Duration,
    pub fps: f32,

    pub(crate) commands: &'a mut Vec<S::Command>,
    pub(crate) clipboard: Option<&'a mut ClipboardSupport>,
    pub(crate) input: &'a Input,
    pub(crate) t: PhantomData<T>,
    pub(crate) screen: (f32, f32, f32),
    pub(crate) renderer: &'a mut imgui_wgpu::Renderer,
}

impl<'a, T: ContextType, S: AppState> Deref for AppContext<'a, T, S> {
    type Target = Input;
    fn deref(&self) -> &Self::Target {
        self.input
    }
}

impl<'a, T: ContextType, S: AppState> AppContext<'a, T, S> {
    pub fn command(&mut self, command: S::Command) {
        self.commands.push(command);
    }

    pub fn ctrl_shortcut_pressed(&self, key: Key) -> bool {
        self.input.held_control() && self.input.key_pressed(key)
    }

    pub fn clipboard_set(&mut self, text: &str) {
        if let Some(cb) = &mut self.clipboard {
            cb.set(text);
        }
    }

    pub fn clipboard_get(&mut self) -> Option<String> {
        self.clipboard.as_mut().and_then(|cb| cb.get())
    }
}

impl<'a, S: AppState> AppContext<'a, Update, S> {
    pub fn file_dialog_open(&self, filter_list: Option<&str>, default_path: Option<&str>) -> Option<PathBuf> {
        if let Ok(nfd::Response::Okay(dir)) = nfd::open_file_dialog(filter_list, default_path) {
            Some(PathBuf::from(dir))

        } else {
            None
        }
    }

    pub fn file_dialog_save(&self, filter_list: Option<&str>, default_path: Option<&str>) -> Option<PathBuf> {
        if let Ok(nfd::Response::Okay(dir)) = nfd::open_save_dialog(filter_list, default_path) {
            Some(PathBuf::from(dir))

        } else {
            None
        }
    }
}

impl<'a, S: AppState> AppContext<'a, Render, S> {
    pub fn pixel_to_screen_rect(&self, pos: [f32; 2], size: [f32; 2]) -> ([f32; 2], [f32; 2]) {
        ([
            self.screen.0 + pos[0] * self.screen.2,
            self.screen.1 + pos[1] * self.screen.2

        ], [
            self.screen.0 + size[0] * self.screen.2,
            self.screen.1 + size[1] * self.screen.2
        ])
    }
    pub fn pixel_to_screen_position(&self, pos: [f32; 2], size: [f32; 2]) -> ([f32; 2], [f32; 2]) {
        ([
            self.screen.0 + pos[0] * self.screen.2,
            self.screen.1 + pos[1] * self.screen.2

        ], [
            size[0] * self.screen.2,
            size[1] * self.screen.2
        ])
    }

    pub fn pixel_mouse(&self) -> Option<(i32, i32)> {
        self.pixels.mouse
    }

    pub fn pixel_resize(&mut self, size: (u32, u32)) {
        let (width, height) = size;
        self.pixels.width = width;
        self.pixels.height = height;
        self.pixels.inner.resize_buffer(width, height);
    }

    pub fn pixel_fill(&mut self, color: &[u8; 4]) {
        for p in self.pixels.inner.get_frame().chunks_exact_mut(4) {
            p.copy_from_slice(color);
        }
    }

    pub fn pixel_size(&self) -> (u32, u32) {
        (self.pixels.width, self.pixels.height)
    }

    pub fn pixel_frame(&mut self) -> &mut [u8] {
        self.pixels.inner.get_frame()
    }

    pub fn with_pixels_mut<C: FnMut(i32, i32, Pixel), T: Iterator<Item=(i32, i32)>>(&mut self, pixels: T, mut callback: C) {
        for (x, y) in pixels {
            if x >= 0 && x < self.pixels.width as i32 && y >= 0 && y < self.pixels.height as i32 {
                let i = (x as usize + y as usize * self.pixels.width as usize) * 4;
                let frame = self.pixels.inner.get_frame();
                callback(x, y, Pixel(&mut frame[i..i + 4]))
            }
        }
    }

    pub fn with_pixel_mut<C: FnMut(Pixel)>(&mut self, x: i32, y: i32, mut callback: C) {
        if x >= 0 && x < self.pixels.width as i32 && y >= 0 && y < self.pixels.height as i32 {
            let i = (x as usize + y as usize * self.pixels.width as usize) * 4;
            let frame = self.pixels.inner.get_frame();
            callback(Pixel(&mut frame[i..i + 4]))
        }
    }

    pub fn pixel_mut(&mut self, x: i32, y: i32) -> Option<Pixel> {
        if x >= 0 && x < self.pixels.width as i32 && y >= 0 && y < self.pixels.height as i32 {
            let i = (x as usize + y as usize * self.pixels.width as usize) * 4;
            let frame = self.pixels.inner.get_frame();
            Some(Pixel(&mut frame[i..i + 4]))

        } else {
            None
        }
    }

    fn update_texture(&self, id: imgui::TextureId, data: &[u8]) {
        if let Some(tex) = self.renderer.textures.get(id) {
            tex.write(&self.pixels.inner.context().queue, data, tex.width(), tex.height());
        }
    }

    fn create_texture(&mut self, width: u32, height: u32, data: Vec<u8>) -> UiTexture {
        let context = self.pixels.inner.context();
        let tex = imgui_wgpu::Texture::new(&context.device, self.renderer, imgui_wgpu::TextureConfig {
            size: Extent3d {
                width: width as u32,
                height: height as u32,
                depth_or_array_layers: 1
            },
            label: None,
            format: None,
            usage: TextureUsages::COPY_SRC | TextureUsages::COPY_DST | TextureUsages::TEXTURE_BINDING,
            mip_level_count: 1,
            sample_count: 1,
            dimension: TextureDimension::D2,
            sampler_desc: wgpu::SamplerDescriptor {
                label: Some("nearest neighbor sampler"),
                address_mode_u: wgpu::AddressMode::ClampToEdge,
                address_mode_v: wgpu::AddressMode::ClampToEdge,
                address_mode_w: wgpu::AddressMode::ClampToEdge,
                mag_filter: wgpu::FilterMode::Nearest,
                min_filter: wgpu::FilterMode::Nearest,
                mipmap_filter: wgpu::FilterMode::Nearest,
                lod_min_clamp: -100.0,
                lod_max_clamp: 100.0,
                compare: None,
                anisotropy_clamp: None,
                border_color: None,
            }
        });
        tex.write(&context.queue, &data, width as u32, height as u32);
        UiTexture::Initialized {
            id: self.renderer.textures.insert(tex),
            width,
            height,
            data
        }
    }
}

pub struct Pixel<'a>(&'a mut [u8]);
impl<'a> Pixel<'a> {
    pub fn set(&mut self, rgba: &[u8; 4]) {
        self.0.copy_from_slice(rgba);
    }
}

pub struct PixelContext {
    pub(crate) inner: pixels::Pixels,
    pub(crate) mouse: Option<(i32, i32)>,
    pub(crate) width: u32,
    pub(crate) height: u32
}

pub enum UiTexture {
    Uninitialized(u32, u32, [u8; 4]),
    Initialized {
        id: imgui::TextureId,
        width: u32,
        height: u32,
        data: Vec<u8>
    }
}

impl UiTexture {
    pub fn new(width: u32, height: u32, fill_color: [u8; 4]) -> Self {
        Self::Uninitialized(width, height, fill_color)
    }

    fn ensure_init<T: AppState>(&mut self, ctx: &mut AppContext<Render, T>) {
        if let Self::Uninitialized(width, height, color) = self {
            let width = *width;
            let height = *height;
            let mut data: Vec<u8> = std::iter::repeat(0).take(width as usize * height as usize * 4).collect();
            for d in data.chunks_exact_mut(4) {
                d.copy_from_slice(color);
            }
            *self = ctx.create_texture(width, height, data);
        }
    }

    pub fn id<T: AppState>(&mut self, ctx: &mut AppContext<Render, T>) -> imgui::TextureId {
        self.ensure_init(ctx);
        if let Self::Initialized { id, .. } = self {
            *id

        } else {
            unreachable!("texture initialization missing");
        }
    }

    pub fn buffer<'a, 'b, T: AppState>(&'a mut self, ctx: &'a mut AppContext<'b, Render, T>) -> UiTextureBuffer<'a, 'b, T> {
        self.ensure_init(ctx);
        if let Self::Initialized { id, width, height, data } = self {
            UiTextureBuffer {
                ctx,
                id: *id,
                queued: false,
                width: *width as u32,
                height: *height as u32,
                data
            }

        } else {
            unreachable!("texture initialization missing");
        }
    }
}

pub struct UiTextureBuffer<'a, 'b, T: AppState> {
    id: imgui::TextureId,
    ctx: &'a mut AppContext<'b, Render, T>,
    queued: bool,
    pub width: u32,
    pub height: u32,
    pub data: &'a mut [u8],
}

impl<'a, 'b, T: AppState> UiTextureBuffer<'a, 'b, T> {
    pub fn queue(&mut self) {
        self.queued = true;
    }
}

impl<'a, 'b, T: AppState> Drop for UiTextureBuffer<'a, 'b, T> {
    fn drop(&mut self) {
        if self.queued {
            self.ctx.update_texture(self.id, self.data);
        }
    }
}

#[derive(Clone)]
pub struct AudioContext {
    queue: Arc<ArrayQueue<f32>>,
    silent: Arc<AtomicBool>,
    stream: Arc<Mutex<Option<Stream>>>
}

impl AudioContext {
    pub(crate) fn new(buffer_size: usize) -> Self {
        Self {
            queue: Arc::new(ArrayQueue::<f32>::new(buffer_size)),
            silent: Arc::new(AtomicBool::new(false)),
            stream: Arc::new(Mutex::new(None))
        }
    }

    pub fn initialize(&mut self, sample_rate: u32, channels: u16) -> Result<(), Box<dyn std::error::Error>> {
        let host = cpal::default_host();
        let device = host.default_output_device().ok_or_else(|| "".to_string())?;

        let mut supported_configs_range = device.supported_output_configs()?;
        let supported_config = supported_configs_range.find(|c| c.channels() == channels && c.sample_format() == SampleFormat::F32).ok_or_else(|| "".to_string())?;
        let supported_config = supported_config.with_sample_rate(SampleRate(sample_rate));

        let err_fn = |err| eprintln!("an error occurred on the output audio stream: {}", err);
        let config = supported_config.into();

        let handle = (self.queue.clone(), self.silent.clone());
        let mut last_value = 0.0;
        let stream = device.build_output_stream(&config, move |data: &mut [f32], _: &cpal::OutputCallbackInfo| {
            AudioContext::write(&handle, &mut last_value, data)

        }, err_fn)?;
        stream.play()?;

        if let Ok(mut s) = self.stream.lock() {
            *s = Some(stream);
        }
        Ok(())
    }

    pub fn push_sample(&self, left: f32, right: f32) {
        self.queue.push(left).ok();
        self.queue.push(right).ok();
    }

    pub fn set_silent(&self, silent: bool) {
        while self.queue.pop().is_some() {}
        self.silent.store(silent, std::sync::atomic::Ordering::Relaxed);
    }

    fn write<T: Sample>(handle: &(Arc<ArrayQueue<f32>>, Arc<AtomicBool>), last_value: &mut f32, output: &mut [T]) {
        let (queue, silent) = handle;
        if silent.load(std::sync::atomic::Ordering::Relaxed) {
            for sample in output.iter_mut() {
                queue.pop();
                *sample = Sample::from(&0f32);
            }

        } else {
            for sample in output.iter_mut() {
                *last_value = queue.pop().unwrap_or(*last_value);
                *sample = Sample::from(last_value);
            }
        }
    }
}


// Clipboard Integration ------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct ClipboardSupport(ClipboardContext);
impl ClipboardSupport {
    pub fn init() -> Option<Self> {
        ClipboardContext::new().ok().map(Self)
    }
}

impl ClipboardSupport {
    pub fn get(&mut self) -> Option<String> {
        self.0.get_contents().ok()
    }

    pub fn set(&mut self, text: &str) {
        let _ = self.0.set_contents(text.to_owned());
    }
}

