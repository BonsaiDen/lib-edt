// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use edt::{
    App, AppContext, AppConfig, Update, Render, RenderStyle, UiTexture,
    drw::{Color, PxlCanvasBuffer, Rotation},
    imgui::Ui
};


// Application ----------------------------------------------------------------
// ----------------------------------------------------------------------------
type State = ();

struct Basic {
    texture: UiTexture,
    r: f32
}

impl App<State> for Basic {
    fn defaults(_state: &State) -> AppConfig {
        AppConfig {
            window_title: "Basic",
            window_size: (640, 480, true),
            style: RenderStyle::Pixels(160, 120),
            resizeable: true,
            .. AppConfig::default()
        }
    }

    fn render(&mut self, mut ctx: AppContext<Render, State>, ui: &Ui) {
        let size = ctx.pixel_size();
        ctx
            .pixel_frame()
            .pxl_canvas(size)
            .color(Color::BLACK)
            .clear()
            .color(Color::RED)
            .push()
                .translate((size.0 as f32 * 0.5, size.1 as f32 * 0.5))
                .rotate(Rotation::Deg(self.r))
                .rect((0.0, 0.0), (32.0, 32.0))
                .pop();

        imgui::Window::new("Window").size([192.0, 192.0], imgui::Condition::Appearing).build(ui, || {
            ui.text("imgui texture");
            {
                let mut buffer = self.texture.buffer(&mut ctx);
                buffer.data
                    .pxl_canvas((buffer.width, buffer.height))
                    .color(Color::PINK)
                    .clear()
                    .color(Color::WHITE)
                    .push()
                        .translate((64.0, 64.0))
                        .rotate(Rotation::Deg(self.r))
                        .rect((0.0, 0.0), (32.0, 32.0))
                        .pop();
                buffer.queue();
            }
            let list = ui.get_window_draw_list();
            let p = ui.cursor_screen_pos();
            list.add_image(self.texture.id(&mut ctx), [p[0], p[1]], [p[0] + 128.0, p[1] + 128.0]).build();
        });
        self.r += 1.0;
    }
}

fn main() {
    let basic = Basic {
        texture: UiTexture::new(128, 128, [0, 0, 0, 0]),
        r: 0.0
    };
    basic.run().expect("Basic failed");
}

