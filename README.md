# edt

A support library for other projects that provides 2D rendering, audio and gui functionality using a
selection of 3rd party libraries such as:

- wgpu
- imgui
- pixels
- cpal

## License

Licensed under either of
 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)
at your option.

